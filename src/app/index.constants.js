/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('ia')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
