(function() {
  'use strict';

  angular
    .module('ia')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })  
      .state('listas', {
        url: '/listas',
        templateUrl: 'app/estructuras/listas.html',
        controller: 'ListController',
        controllerAs: 'vm'
      })
      .state('pilas', {
        url: '/pilas',
        templateUrl: 'app/estructuras/pilas.html',
        controller: 'PileController',
        controllerAs: 'pl'
      })
      .state('colas', {
        url: '/colas',
        templateUrl: 'app/estructuras/colas.html',
        controller: 'ColasController',
        controllerAs: 'cl'
      })
      /*-------------------------------------arreglos------------------*/
      .state('arreglos_unidimensionales', {
        url: '/arreglos_unidimensionales',
        templateUrl: 'app/estructuras/arreglos_unidimensionales.html',
        controller: 'arrayUnidemesionalController',
        controllerAs: 'arrayUni'
      })
      .state('arreglos_bidimensionales', {
        url: '/arreglos_bidimensionales',
        templateUrl: 'app/estructuras/arreglos_bidimensionales.html',
        controller: 'arrayBidimensionalesController',
            controllerAs: 'arrayBi'
      })
      .state('arreglos_tres_mas_dimensiones', {
        url: '/arreglos_tres_mas_dimensiones',
        templateUrl: 'app/estructuras/arreglos_tres_mas_dimensiones.html',
        controller: 'arrayTresMasDimensiones',
        controllerAs: 'arrayTresD'
      })
      /*-----------------------TABLAS HASH-------------------------------*/
      .state('tablasHash', {
        url: '/tablasHash',
        templateUrl: 'app/estructuras/tablasHash.html',
        controller: 'tablasHashController',
        controllerAs: 'tablasH'
      })
      //ARBOLES --------------------------------------------------
      .state('arbolBinario', {
        url: '/arbolBinario',
        templateUrl: 'app/arboles/arbolBinario.html',
        controller: 'arbolBinarioController',
        controllerAs: 'arblBirio'
      })
      .state('arbolBinarioBusqueda', {
        url: '/arbolBinarioBusqueda',
        templateUrl: 'app/arboles/arbolBinarioBusqueda.html',
        controller: 'MainController',
        controllerAs: 'MainController'
      })
      .state('arbolAVL', {
        url: '/arbolAVL',
        templateUrl: 'app/arboles/arbolAVL.html',
        controller: 'MainController',
        controllerAs: 'MainController'
      })
      .state('arbolRojiNegro', {
        url: '/arbolRojiNegro',
        templateUrl: 'app/arboles/arbolRojiNegro.html',
        controller: 'MainController',
        controllerAs: 'MainController'
      })
      .state('arbolSplay', {
        url: '/arbolSplay',
        templateUrl: 'app/arboles/arbolSplay.html',
        controller: 'MainController',
        controllerAs: 'MainController'
      })
      .state('arbolHeap', {
        url: '/arbolHeap',
        templateUrl: 'app/arboles/arbolHeap.html',
        controller: 'MainController',
        controllerAs: 'MainController'
      })
      //TEORIA DE GRAFOS+---------------------------------------------
      .state('teoriaDeGrafos', {
        url: '/teoriaDeGrafos',
        templateUrl: 'app/grafos/teoriaDeGrafos.html',
        controller: 'grafosController',
        controllerAs: 'grafControll'
      })
    $urlRouterProvider.otherwise('/');
  }
})();
