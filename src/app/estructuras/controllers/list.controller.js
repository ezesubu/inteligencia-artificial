(function() { 
    'use strict';

    angular
        .module('ia')
        .controller('ListController', ListController);

    /** @ngInject */
    function ListController() {
        // Local Variables
        //var objValidator = new validationService();
        var vm = this;

        // Bindables

        vm.fnAdd = fnAdd;
        vm.fnAdd_principio = fnAdd_principio;
        vm.fnAdd_orden = fnAdd_orden;
        vm.fnDelete = fnDelete;

        vm.mylist = [];
        vm.mylist2 = [];

        // AutoInit
        fnInit();

        // Methods
        function fnInit() {
            //ToDo Generar 10 numeros aleatorios hasta 999 y agregarls al array
            //Ng repeat hacerlo bonitò
            fnSetValueInput();
            for (var i = 0; i < 5; i++) {
                vm.mylist.push(fnnumerosAleatorios());
                vm.mylist2.push(fnnumerosAleatorios());
            }
        }

        function fnnumerosAleatorios() {
            return Math.round(Math.random() * 1000);
        }
        //----------------FUNCIONES PARA INSERTAR --------------
        function fnAdd() { //AGREGAR AL FINAL
            vm.mylist.push(parseInt(vm.newValue));
            vm.newValue = fnnumerosAleatorios();
        }

        function fnAdd_principio() { //AGREGAR AL PRINCIPIO
            vm.mylist.unshift(parseInt(vm.newValue));
            fnSetValueInput();
            console.log(vm.mylist);
        }

        function fnAdd_orden() { //ORDENAR DE MENOR A MAYOR
            vm.mylist = vm.mylist.sort(fnnemorAmayor());
        }

        function fnnemorAmayor(num1, num2) {
            return num1 - num2;
        }
        //--------------------FUNCIONES PARA ELIMINAR ---------------------------------
        function fnDelete() { //ELIMINAR
            var num = vm.newValue2;
            console.log(vm.mylist);
            vm.mylist.splice(vm.mylist.indexOf(parseInt(num)), 1);
            console.log(vm.mylist);
            vm.newValue2 = null;
        }

        function fnSetValueInput() {
            vm.newValue = fnnumerosAleatorios();
        }
    }
})();
