(function() {
    'use strict';

    angular
        .module('ia')
        .controller('ColasController', ColasController);

    /** @ngInject */
    function ColasController() {
        // Local Variables
        //var objValidator = new validationService();
        var cl = this;

        // Bindables
        cl.fnInsertar = fnInsertar;
        cl.fnEliminar = fnEliminar;

        cl.mylist = [];

        // AutoInit
        fnInit();

        // Methods
        function fnInit() {
            for (var i = 0; i < 5; i++) {
                cl.mylist.push(fnNumerosAleatorios());
            }
            cl.newValue = fnNumerosAleatorios();
        }

        function fnNumerosAleatorios() {
            return Math.round(Math.random() * 1000);
        }

        /*------------------------------------------APILAR--------*/
        function fnInsertar() {
            cl.mylist.push(parseInt(cl.newValue));
            cl.newValue = fnNumerosAleatorios();
        }

        function fnEliminar() {
            cl.mylist.shift();
            cl.newValue = cl.mylist[0];
        }
    }
})();
