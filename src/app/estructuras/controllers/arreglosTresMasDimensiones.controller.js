(function ()
{
    'use strict';

    angular
        .module('ia')
        .controller('arrayTresMasDimensiones', arrayTresMasDimensiones);

    /** @ngInject */
    function arrayTresMasDimensiones()
    {
      // Local Variables
      //var objValidator = new validationService();
      var arrayTresD = this;

      // Bindables
      arrayTresD.fnInsertar = fnInsertar;
  
      arrayTresD.mylist = [];
      arrayTresD.indice = [];

      // AutoInit
      fnInit();

      // Methods
      function fnInit() 
      {
        var array;
        array = [
                 [[fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                  [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                  [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                  [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()]
                 ],
                 [
                  [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                  [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                  [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                  [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()]
                 ],
                 [
                  [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                  [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                  [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                  [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()]
                 ],
                 [
                  [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                  [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                  [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                  [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()]
                 ]
                ];
        arrayTresD.grid = array;
        indicesAleatorios();
      }
      function fnNumerosAleatorios()
      {
        return Math.round(Math.random()*100) ;
      }
      function fnNumerosAleatorios0a3()
      {
        return Math.round(Math.random()*3) ;
      }       
      function fnInsertar()
      {
        try
        {
          arrayTresD.grid[arrayTresD.indice_1][arrayTresD.indice_2][arrayTresD.indice_3] = parseInt(arrayTresD.newValue);
          arrayTresD.newValue = null;
          /*arrayTresD.grid[arrayTresD.indice_1][arrayTresD.indice_2] = arrayTresD.newValue;*/
        }
        catch(e)
        {
          window.alert(" Los indices deben contener un numero entre 0 a 3.");
        }
        indicesAleatorios();
      }
      function indicesAleatorios()
      {
        arrayTresD.indice_1 = fnNumerosAleatorios0a3();
        arrayTresD.indice_2 = fnNumerosAleatorios0a3();
        arrayTresD.indice_3 = fnNumerosAleatorios0a3();
        arrayTresD.newValue = fnNumerosAleatorios();
      }
      function fnArrays()
      {
        var array = [];
        try
        {
          array = [
                   [[fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                    [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                    [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                    [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()]
                   ],
                   [
                    [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                    [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                    [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                    [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()]
                   ],
                   [
                    [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                    [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                    [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                    [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()]
                   ]
                  ];
        }
        catch(e)
        {
          window.alert(" Se ha geredado un error al crear el arreglo multidimencional");
        }
        return array;
      }
    }
})();