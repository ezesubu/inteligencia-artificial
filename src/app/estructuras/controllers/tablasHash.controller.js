(function ()
{
    'use strict';

    angular
        .module('ia')
        .controller('tablasHashController', tablasHashController);

    /** @ngInject */
    function tablasHashController()
    {
      // Local Variables
      //var objValidator = new validationService();
      var tablasH = this;
      /*tablasH.insertar = true;*/

      // Bindables
      tablasH.fnInsertar = fnInsertar;
     
      tablasH.fnCambiarEstado = fnCambiarEstado;

      tablasH.hash = new Object();
      
      // AutoInit
      fnInit();

      // Methods
      function fnInit() 
      {
        
        tablasH.hash["uno"] = fnNumerosAleatorios();
        tablasH.hash["dos"] = fnNumerosAleatorios();
        tablasH.hash["tres"] = fnNumerosAleatorios();
        tablasH.hash["cuatro"] = fnNumerosAleatorios();
        /*console.log(tablasH.hash);*/
       
      }
      function fnNumerosAleatorios()
      {
        return Math.round(Math.random()*100) ;
      }
      function fnNumerosAleatorios0a3()
      {
        return Math.round(Math.random()*3) ;
      }       
      function fnInsertar()
      {

        try
        {
          if (tablasH.insertar == "insertar")
          {
           fninsertarHash();
          }
          else if (tablasH.eliminar == "eliminar")
          {
            fneliminarHash();
          }
          else if (tablasH.actualizar == "actualizar") 
          {
            fnActualizar();
          }
          else
          {
            window.alert("Seleccione unaa opción.");
          }
        }
        catch(ex)
        {
          window.alert(ex);
        }        
      }
      function fninsertarHash()
      {
        tablasH.hash[tablasH.indice_1] = tablasH.newValue;
        tablasH.indice_1 = null;
        tablasH.newValue = null;
      }
      function fneliminarHash()
      {
        if (tablasH.hash.hasOwnProperty(tablasH.indice_1))
        {
          delete tablasH.hash[tablasH.indice_1];
          tablasH.indice_1 =null;
        }
        else
        {
          window.alert("El key digitado no existe.");
        }       
      }
      function fnActualizar()
      {
        if (tablasH.hash.hasOwnProperty(tablasH.indice_1))
        {
          tablasH.hash[tablasH.indice_1] = tablasH.newValue;
          tablasH.indice_1 = null;
          tablasH.newValue = null;
        }
        else
        {
          window.alert("El key digitado no existe.");
        }
      }
      function fnArrays()
      {
        try
        {

        }
        catch(e)
        {
          window.alert(" Se ha geredado un error al crear el arreglo multidimencional");
        }
        return array;
      }
      function fnCambiarEstado()
      {
        console.log(tablasH.insertar);
      }
    }
})();