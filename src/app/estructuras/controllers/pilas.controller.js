(function() {
    'use strict';

    angular
        .module('ia')
        .controller('PileController', PileController);

    /** @ngInject */
    function PileController() {
        // Local Variables
        //var objValidator = new validationService();
        var pl = this;

        // Bindables
        pl.fnApilar = fnApilar;
        pl.fnDesApilar = fnDesApilar;


        pl.mylist = [];


        // AutoInit
        fnInit();

        // Methods
        function fnInit() {
            //pl.mylist.push(1);
            for (var i = 0; i < 5; i++) {
                pl.mylist.push(fnnumerosAleatorios());
            }
        }

        function fnnumerosAleatorios() {
            return Math.round(Math.random() * 1000);
        }

        /*------------------------------------------APILAR--------*/
        function fnApilar() {
            pl.mylist.unshift(parseInt(pl.newValue));
            pl.newValue = null;
        }

        function fnDesApilar() {
            pl.mylist.shift();
        }
    }
})();