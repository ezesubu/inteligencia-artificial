(function ()
{
    'use strict';

    angular
        .module('ia')
        .controller('arrayUnidemesionalController', arrayUnidemesionalController);

    /** @ngInject */
    function arrayUnidemesionalController()
    {
      // Local Variables
      //var objValidator = new validationService();
      var arrayUni = this;
      // Bindables
      arrayUni.fnInsertar=fnInsertar;
      arrayUni.fnEliminar=fnEliminar;

      arrayUni.mylist = [];
      arrayUni.indice= [];
  
      // AutoInit
      fnInit();

      // Methods
      function fnInit() {
        for (var i = 0; i < 5; i++) {
          arrayUni.mylist.push(fnNumerosAleatorios(1000));
          arrayUni.indice.push(i);
        }
        fnAsignarValores();
      }
      function fnAsignarValores()
      {
        arrayUni.indice_eliminar = fnNumerosAleatorios(arrayUni.mylist.length-1);
        //arrayUni.newValue = arrayUni.mylist[0];
      }
      function fnNumerosAleatorios(a)
      {
        return Math.round(Math.random()*a) ;
      }      
      /*------------------------------------------APILAR--------*/
      function fnInsertar()
      {
        arrayUni.mylist.splice(arrayUni.indice_eliminar,0,arrayUni.newValue);
        arrayUni.indice_eliminar=null;
        arrayUni.newValue=null;
        fnindices();
        fnAsignarValores();
      }
      function fnEliminar()
      {
        if (arrayUni.indice_eliminar == null) {
            window.alert("Debes digitar el indice del valor a elminar.");
        } else {
            arrayUni.mylist.splice(arrayUni.indice_eliminar,1);
            arrayUni.indice_eliminar=null;  
            fnindices();
            fnAsignarValores();
        }
      }
      function fnindices()
      {
        arrayUni.indice=[];
        for (var i = 0; i < arrayUni.mylist.length; i++)
        {          
          arrayUni.indice.push(i);
        }
      }
    }
})();