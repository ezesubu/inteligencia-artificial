(function ()
{
    'use strict';

    angular
        .module('ia')
        .controller('arrayBidimensionalesController', arrayBidimensionalesController);

    /** @ngInject */
    function arrayBidimensionalesController()
    {
      // Local Variables
      //var objValidator = new validationService();
      var arrayBi = this;

      // Bindables
      arrayBi.fnInsertar = fnInsertar;
  
      arrayBi.mylist = [];
      arrayBi.indice = [];

      // AutoInit
      fnInit();

      // Methods
      function fnInit() {
        arrayBi.name = 'World';
        arrayBi.grid = [[fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()],
                [fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios(),fnNumerosAleatorios()]];
      }
      function fnNumerosAleatorios()
      {
        return Math.round(Math.random()*100) ;
      }      
      function fnInsertar()
      {
        try
        {
          arrayBi.grid[arrayBi.indice_1][arrayBi.indice_2] = arrayBi.newValue;
        }
        catch(e)
        {
        }
        arrayBi.indice_1 = null;
        arrayBi.indice_2 = null;
        arrayBi.newValue = null;
      }
      function fnindices()
      {
        arrayBi.indice=[];
        for (var i = 0; i < arrayBi.mylist.length; i++)
        {          
          arrayBi.indice.push(i);
        }
      }
    }
})();