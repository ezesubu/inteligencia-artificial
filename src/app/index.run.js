(function() {
    'use strict';

    angular
        .module('ia')
        .run(runBlock);

    /** @ngInject */
    function runBlock($log) {

        $log.debug('runBlock end');
    }

})();