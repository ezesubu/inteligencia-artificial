(function() {
    'use strict';

    angular 
        .module('ia')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController($timeout, webDevTec, toastr) {
        var vm = this;

        vm.awesomeThings = [];
        vm.classAnimation = '';
        vm.creationDate = 1476902576102;
        vm.showToastr = showToastr;
       /* vm.CarouselDemoCtrl = CarouselDemoCtrl;
        vm.myInterval = 0;
        vm.slides = [];*/

            activate();

        function activate() {
            getWebDevTec();
            $timeout(function() {
                vm.classAnimation = 'rubberBand';
            }, 4000);

            //CarouselDemoCtrl();
        }

        function showToastr() {
            toastr.info('<a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>Estructuta de datos UDEC</b></a>');
            vm.classAnimation = '';
        }

        function getWebDevTec() {
            vm.awesomeThings = webDevTec.getTec();

            angular.forEach(vm.awesomeThings, function(awesomeThing) {
                awesomeThing.rank = Math.random();
            });
        }


        /*function CarouselDemoCtrl() {
            console.log("entro al carrusel");
            vm.myInterval = 3000;
            try
            {
                vm.slides = [
                             {image: "assets/images/udec.png"},
                             {image: "assets/images/gulp.png"},
                             {image: "assets/images/jasmine.png"},
                             {image: "assets/images/karma.png"}
                            ];
            }catch (e) 
            {
                console.log(e);
            }
            console.log(vm.slides);
        }*/
    }
})();
