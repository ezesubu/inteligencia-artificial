(function() {
    'use strict';

    angular
        .module('ia')
        .controller('arbolBinarioController', arbolBinarioController);

    /** @ngInject */
    function arbolBinarioController() {
        // Local Variables
        //var objValidator = new validationService();
        var arblBirio = this;

        /*arblBirio.insertar = true;*/
        arblBirio.crear = fncrear;
        arblBirio.PrinInOrden = fnImprimirEnOrden;
        arblBirio.PrinPreOrden = fnImprimirPreOrden;
        arblBirio.PrinPostOrden = fnImprimirPostOrden;
        arblBirio.PrinHeight = fnImprimirAltura;
        arblBirio.Balanced = fnImprimirEquilibrado;


        // Bindables
        arblBirio.nodos = [];
        // AutoInit
        fnInit();

        function fnInit() {
            arblBirio.nodos = [1, 23, 5, 3, 9, 2, 39];
        }

        // Methods
        function fnImprimirEquilibrado() {
            var num = arblBirio.nodos;
            try {
                num = num.split(',');
            } catch (ex) {}
            fnclearLogDiv();
            var bst = new BinarySearchTree();
            var h = 90;
            for (var i = 0; i < num.length; i++) {
                bst.add(parseInt(num[i]));
                if (i == (num.length - 1)) {
                    $("#printLogDiv").append('<div class="panel" style="position:relative;height:' + (h * (bst.height())) + 'px" id="tree' + i + '"></div>');
                    bst.display("tree" + i);
                }
            }
            bst.printIsBalanced();
        }

        function fnImprimirAltura() {
            var num = arblBirio.nodos;
            try {
                num = num.split(',');
            } catch (ex) {}
            fnclearLogDiv();
            var bst = new BinarySearchTree();
            var h = 90;
            for (var i = 0; i < num.length; i++) {
                bst.add(parseInt(num[i]));
                if (i == (num.length - 1)) {
                    $("#printLogDiv").append('<div class="panel" style="position:relative;height:' + (h * (bst.height())) + 'px" id="tree' + i + '"></div>');
                    bst.display("tree" + i);
                }
            }
            bst.printHeight();
        }

        function printMsg(val) {
            $("#printLogTable").append('<tr><td colspan="2"><h4>' + val + '</h4></td></tr>');
        }

        function fnImprimirPostOrden() {
            var num = arblBirio.nodos;
            try {
                num = num.split(',');
            } catch (ex) {}
            fnclearLogDiv();
            var bst = new BinarySearchTree();
            var h = 90;
            for (var i = 0; i < num.length; i++) {
                bst.add(parseInt(num[i]));
                if (i == (num.length - 1)) {
                    $("#printLogDiv").append('<div class="panel" style="position:relative;height:' + (h * (bst.height())) + 'px" id="tree' + i + '"></div>');
                    bst.display("tree" + i);
                }
            }
            bst.printPostOrder();
        }

        function fnImprimirPreOrden() {
            var num = arblBirio.nodos;
            try {
                num = num.split(',');
            } catch (ex) {}
            fnclearLogDiv();
            var bst = new BinarySearchTree();
            var h = 90;
            for (var i = 0; i < num.length; i++) {
                bst.add(parseInt(num[i]));
                if (i == (num.length - 1)) {
                    $("#printLogDiv").append('<div class="panel" style="position:relative;height:' + (h * (bst.height())) + 'px" id="tree' + i + '"></div>');
                    bst.display("tree" + i);
                }
            }
            bst.printPreOrder();
        }

        function fnclearLogDiv() {
            $("#printLogDiv").html('');
            $("#printLogTable").html('');
            $('#demopanel a:first').tab('show');
            $('html,body').animate({
                    scrollTop: ($("#demopanel").offset().top)
                },
                'slow');
        }

        function printInsertionSortStep(arr, first, last, msg) {
            var text = '<table class="table table-bordered"><tr>';
            for (var i = 0; i < arr.length; i++) {
                if ((first == i) || (last == i)) {
                    text += '<td class="tdgridHilight">' + arr[i] + '</td>';
                } else {
                    text += '<td>' + arr[i] + '</td>';
                }
            }
            text += '</tr></table>';
            $("#printLogTable").append('<tr><td>' + msg + '</td>' + '<td>' + text + '</td></tr>');
        }

        function fnImprimirEnOrden() {

            var num = arblBirio.nodos;
            try {
                num = num.split(',');
            } catch (ex) {}
            fnclearLogDiv();
            var bst = new BinarySearchTree();
            var h = 90;
            for (var i = 0; i < num.length; i++) {
                bst.add(parseInt(num[i]));
                if (i == (num.length - 1)) {
                    $("#printLogDiv").append('<div class="panel" style="position:relative;height:' + (h * (bst.height())) + 'px" id="tree' + i + '"></div>');
                    bst.display("tree" + i);
                }
            }
            bst.printInOrder();
        }

        function fncrear() {
            var num = arblBirio.nodos;
            try {
                num = num.split(',');
            } catch (ex) {}

            var bst = new BinarySearchTree();
            fnclearLogDiv();
            var h = 90;
            for (var i = 0; i < num.length; i++) {
                bst.add(parseInt(num[i]));
                $("#printLogDiv").append('<div class="panel" style="position:relative;height:' + (h * (bst.height())) + 'px" id="tree' + i + '"><span class="treeNote">Insertar nodo ' + num[i] + '</span></div>');
                bst.display("tree" + i);
            }
        }

        function drawEdge(divid, x1, y1, x2, y2) {
            // distance
            var length = Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
            // center
            var cx = ((x1 + x2) / 2) - (length / 2);
            var cy = ((y1 + y2) / 2) - 1;
            // angle
            var angle = Math.atan2((y1 - y2), (x1 - x2)) * (180 / Math.PI);
            // make hr
            var htmlLine = "<div style='padding:0px; margin:0px; height:2px; background-color:#000; line-height:1px; position:absolute; left:" + cx + "px; top:" + cy + "px; width:" + length + "px; -moz-transform:rotate(" + angle + "deg); -webkit-transform:rotate(" + angle + "deg); -o-transform:rotate(" + angle + "deg); -ms-transform:rotate(" + angle + "deg); transform:rotate(" + angle + "deg);' />";
            $("#" + divid).append(htmlLine);
        }

        function BinarySearchTree() {
            this._root = null;
        }

        BinarySearchTree.prototype = {

            constructor: BinarySearchTree,

            add: function(value) {
                var node = {
                        value: value,
                        left: null,
                        right: null,
                        posx: 262,
                        posy: 10
                    },
                    current;
                var count = 0,
                    posx, mcon = 400,
                    hcon = 90;

                if (this._root === null) {
                    this._root = node;
                } else {
                    current = this._root;

                    while (true) {
                        count++;
                        posx = current.posx;
                        if (value < current.value) {

                            if (current.left === null) {
                                node.posx = posx - mcon * Math.pow(0.5, count);
                                node.posy += count * hcon;
                                current.left = node;
                                break;
                            } else {
                                current = current.left;
                            }

                        } else if (value > current.value) {

                            if (current.right === null) {
                                node.posx = posx + mcon * Math.pow(0.5, count);
                                node.posy += count * hcon;
                                current.right = node;
                                break;
                            } else {
                                current = current.right;
                            }

                        } else {
                            break;
                        }
                    }
                }
            },


            remove: function(value) {
                var found = false,
                    parent = null,
                    current = this._root,
                    childCount,
                    replacement,
                    replacementParent;

                while (!found && current) {
                    if (value < current.value) {
                        parent = current;
                        current = current.left;
                    } else if (value > current.value) {
                        parent = current;
                        current = current.right;
                    } else {
                        found = true;
                    }
                }

                if (found) {
                    childCount = (current.left !== null ? 1 : 0) +
                        (current.right !== null ? 1 : 0);

                    if (current === this._root) {
                        switch (childCount) {
                            case 0:
                                this._root = null;
                                break;

                            case 1:
                                this._root = (current.right === null ?
                                    current.left : current.right);
                                break;

                            case 2:

                                replacement = this._root.left;

                                while (replacement.right !== null) {
                                    replacementParent = replacement;
                                    replacement = replacement.right;
                                }


                                if (replacementParent !== null) {

                                    replacementParent.right = replacement.left;
                                    replacement.right = this._root.right;
                                    replacement.left = this._root.left;
                                } else {
                                    replacement.right = this._root.right;
                                }

                                this._root = replacement;
                        }

                    } else {

                        switch (childCount) {

                            case 0:
                                if (current.value < parent.value) {
                                    parent.left = null;

                                } else {
                                    parent.right = null;
                                }
                                break;
                            case 1:
                                if (current.value < parent.value) {
                                    parent.left = (current.left === null ?
                                        current.right : current.left);

                                } else {
                                    parent.right = (current.left === null ?
                                        current.right : current.left);
                                }
                                break;
                            case 2:
                                replacement = this._root.left;

                                while (replacement.right !== null) {
                                    replacementParent = replacement;
                                    replacement = replacement.right;
                                }


                                if (replacementParent !== null) {

                                    replacementParent.right = replacement.left;

                                    replacement.right = this._root.right;
                                    replacement.left = this._root.left;
                                } else {

                                    replacement.right = this._root.right;
                                }

                                this._root = replacement;

                        }

                    }
                }
            },


            contains: function(value) {
                var found = false,
                    current = this._root;
                while (!found && current) {
                    if (value < current.value) {
                        current = current.left;
                    } else if (value > current.value) {
                        current = current.right;
                    } else {
                        found = true;
                    }
                }
                return found;
            },

            traverse: function(process) {
                function inOrder(node) {
                    if (node) {
                        if (node.left !== null) {
                            inOrder(node.left);
                        }
                        process.call(this, node);
                        if (node.right !== null) {
                            inOrder(node.right);
                        }
                    }
                }
                inOrder(this._root);
            },

            preOrderTraverse: function(process) {
                function preOrder(node) {
                    if (node) {
                        process.call(this, node);
                        if (node.left !== null) {
                            preOrder(node.left);
                        }
                        if (node.right !== null) {
                            preOrder(node.right);
                        }
                    }
                }
                preOrder(this._root);
            },

            postOrderTraverse: function(process) {
                function postOrder(node) {
                    if (node) {
                        if (node.left !== null) {
                            postOrder(node.left);
                        }
                        if (node.right !== null) {
                            postOrder(node.right);
                        }
                        process.call(this, node);
                    }
                }
                postOrder(this._root);
            },

            height: function() {
                function findHeight(node) {
                    if (node === null) {
                        return 0;
                    }
                    return 1 + Math.max(findHeight(node.left), findHeight(node.right));
                }
                return findHeight(this._root);
            },

            size: function() {
                var length = 0;

                this.traverse(function(node) {
                    length++;
                });

                return length;
            },

            toArray: function() {
                var result = [];

                this.traverse(function(node) {
                    result.push(node.value);
                });

                return result;
            },

            printInOrder: function() {
                var result = [];

                this.traverse(function(node) {
                    result.push(node.value);
                    printInsertionSortStep(result, -1, -1, "Nodo " + node.value + " analizado");
                });

                return result;
            },

            printPreOrder: function() {
                var result = [];

                this.preOrderTraverse(function(node) {
                    result.push(node.value);
                    printInsertionSortStep(result, -1, -1, "Nodo " + node.value + " analizado");
                });

                return result;
            },

            printPostOrder: function() {
                var result = [];

                this.postOrderTraverse(function(node) {
                    result.push(node.value);
                    printInsertionSortStep(result, -1, -1, "Nodo " + node.value + " analizado");
                });

                return result;
            },

            printHeight: function() {
                function findHeight(node) {
                    if (node === null) {
                        return 0;
                    }
                    var tmp = 1 + Math.max(findHeight(node.left), findHeight(node.right));
                    printMsg("Altura del nodo " + node.value + " es " + tmp);
                    return tmp;
                }
                return findHeight(this._root);
            },

            getBHeight: function(root) {
                if (root == null)
                    return 0;
                var res;
                var left = this.getBHeight(root.left);
                var right = this.getBHeight(root.right);

                if (left == -1 || right == -1) {
                    res = -1;
                } else if (Math.abs(left - right) > 1) {
                    res = -1;
                } else {
                    res = Math.max(left, right) + 1;
                }
                printMsg("Equilibrada altura del Nodo " + root.value + " es " + res);
                return res;
            },

            printIsBalanced: function() {
                if (this._root == null) {
                    printMsg("Arbol equilibrado");
                } else if (this.getBHeight(this._root) == -1) {
                    printMsg("Arbol no equilibrado");
                } else {
                    printMsg("Arbol equilibrado");
                }
            },


            toString: function() {
                return this.toArray().toString();
            },

            display: function(divid) {
                var mynode = this._root;
                printNode(mynode);

                function printNode(node) {
                    if (node) {
                        $("#" + divid).append('<span style="left:' + node.posx + 'px;top:' + node.posy + 'px" class="tnode">' + node.value + '</span>');
                        if (node.left) {
                            drawEdge(divid, node.posx + 15, node.posy + 41, node.left.posx + 20, node.left.posy);
                            printNode(node.left);
                        }
                        if (node.right) {
                            drawEdge(divid, node.posx + 15, node.posy + 41, node.right.posx + 20, node.right.posy);
                            printNode(node.right);
                        }
                    }
                }
            }
        };
    }
})();
